package com.luislain.spring.subscription.servicejpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String email;
    private String firstName; // Optional
    private String gender; // Optional
    private String dateofbirthString;
    private Boolean consentString;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateofbirthString() {
        return dateofbirthString;
    }

    public void setDateofbirthString(String dateofbirthString) {
        this.dateofbirthString = dateofbirthString;
    }

    public Boolean getConsentString() {
        return consentString;
    }

    public void setConsentString(Boolean consentString) {
        this.consentString = consentString;
    }

}