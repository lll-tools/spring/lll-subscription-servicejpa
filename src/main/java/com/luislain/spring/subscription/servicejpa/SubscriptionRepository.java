package com.luislain.spring.subscription.servicejpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "subscription", path = "subscription")
public interface SubscriptionRepository extends PagingAndSortingRepository<Subscription, Long> {

  List<Subscription> findByFirstName(@Param("name") String name);

}